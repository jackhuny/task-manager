<!DOCTYPE html>
<html lang="zh-cn">
<head>
	<meta charset="utf-8">
	<title>Simple Task Manager with AngularJs</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<!-- Latest compiled and minified JQuery	-->
	<script src="../js/jquery-2.1.4.min.js"></script>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

	<!-- Latest compiled and minified AngularJs	-->
	<script src="../js/angular.min.js"></script>

	<link rel="stylesheet" href="../css/main.css">
</head>
<body ng-app="taskApp">
<script>
	function showSuccessToast() {
		$().toastmessage('showSuccessToast', "Success Dialog which is fading away ...");
	}
	$(document).ready(function(){
//        showSuccessToast();
	});
</script>
<div id="container" class="container-fluid" ng-controller="MainController">
	<div class="row" style="padding: 0 10px;">
		<!--Tool Bar-->
		<div class="top-toolbar">
			<form class="form-inline">
				<div class="form-group">
					<button type="button" class="btn btn-default btn-sm" ng-class="refreshCtrl.stop?'':'active'" ng-click="refreshCtrl.stop=!refreshCtrl.stop">
						<div class="glyphicon glyphicon-refresh"></div>Auto Refresh {{refreshCtrl.stop?'OFF':'ON'}}
					</button>
					<button type="button" data-toggle="modal" data-target="#addTaskModal" class="btn btn-primary btn-sm">
						<div class="glyphicon glyphicon-filter"></div>
						Add A Task
					</button>
				</div>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
						<input type="text" class="form-control" id="searchPubName" placeholder="Search Publisher" ng-model="search.pub_name">
					</div>
					<div class="input-group">
						<div class="input-group-addon"><span class="glyphicon glyphicon-list"></span></div>
						<input type="text" class="form-control" id="searchTask" placeholder="Search Task" ng-model="search.task_desc">
					</div>
				</div>
				<div class="form-group">
					<button type="button" class="btn btn-default btn-sm"
							ng-class="(search.is_finish == '!1')?'active':''"
							ng-click="(search.is_finish == '!1')? search.is_finish = '' : search.is_finish = '!1'">
						<div class="glyphicon glyphicon-filter"></div>
						Unfinished Only
					</button>
				</div>
			</form>
		</div>
	</div>
	<div class="row">
		<!-- User notifications-->
		<div class="alert alert-info" style="margin-top: 20px" ng-show="loadingStatusHolder.getTask">Loading Tasks ...</div>
	</div>
	<div class="row" style="padding: 0 10px;">
		<table id="main-table" class="table table-striped table-hover table-responsive">
			<thead id="main-table-header" style="font-weight: bold" ng-show="!loadingStatusHolder.getTask && !errorStatusHolder.main">
			<tr> <!--Table Header-->
				<td style="width: 91px" ng-click="customOrder(orderByDefault)">
					Priority
					<span class="glyphicon" ng-class="reverse?'glyphicon-chevron-down':'glyphicon-chevron-up'" ng-show="predicate === orderByDefault"></span>
				</td>
				<td style="width: 75px" class="hidden-xs" ng-click="customOrder(orderByDate)">
					Date
					<span class="glyphicon" ng-class="reverse?'glyphicon-chevron-down':'glyphicon-chevron-up'" ng-show="predicate === orderByDate"></span>
				</td>
				<td class="" ng-click="customOrder('pub_name')">
					Name
					<span class="glyphicon" ng-class="reverse?'glyphicon-chevron-down':'glyphicon-chevron-up'" ng-show="predicate === 'pub_name'"></span>
				</td>
				<td class="">Details</td>
				<td class="" style="text-align: right"><div class="glyphicon glyphicon-wrench"></div></td>
			</tr>
			</thead>
			<tbody>
			<tr class="task_content" ng-repeat="task in taskList | orderBy:predicate:reverse | filter:search"
				id="task_{{task.id}}"
				ng-class="task.is_finish == 1?'done':''"
				ng-click="showTask(task)">
				<td title="{{ task.id }}" ng-show="task.priority > 2" class="p_top">Top Priority</td>
				<td title="{{ task.id }}" ng-show="task.priority == 2" class="p_vhigh">Very High</td>
				<td title="{{ task.id }}" ng-show="task.priority == 1" class="p_high">High</td>
				<td title="{{ task.id }}" ng-show="task.priority < 0" class="p_low">Low</td>
				<td title="{{ task.id }}" ng-hide="task.priority > 0 || task.priority < 0" class="p_normal">Normal</td>
				<!--<span ng-show="task.priority >= 3">Top</span>-->
				<!--<span ng-show="task.priority == 2">Level 2</span>-->
				<!--<span ng-show="task.priority == 1">Level 1</span>-->
				<!--<span ng-show="task.priority == 0">Normal</span>-->
				<!--<span ng-show="task.priority < 0">Low</span>-->
				<td class="hidden-xs" title="{{ toJsDate(task.pub_date) | date : 'medium' }}">{{ toJsDate(task.pub_date) | date : 'shortDate' }}</td>
				<td>{{ task.pub_name }}</td>
				<td>
					<span style="white-space:pre-wrap;" ng-class="(task.is_finish == 1)?'done_task':''">{{ task.task_desc }}</span>
					<span class="done_task_info" ng-show="task.is_finish == 1"><hr/>Finished @ {{toJsDate(task.finish_date) | date:'medium'}}</span>
				</td>
				<td style="text-align: right; white-space: nowrap">
					<div ng-show="task.is_finish == 1" ng-click="markFinish(task.id,0);$event.stopPropagation();" style="cursor: pointer;display:inline">
						<div class="glyphicon glyphicon-ok" title="Done @ {{toJsDate(task.finish_date) | date:'short'}}"></div>
					</div>
					<div ng-show="task.is_finish != 1" ng-click="markFinish(task.id,1);$event.stopPropagation();" style="cursor: pointer;display:inline">
						<div class="glyphicon glyphicon-time" title="In Progress"></div>
					</div>
					<div class="glyphicon glyphicon-edit" style="cursor:pointer"
						 ng-click="setCurrentEditTask(task);$event.stopPropagation();"></div>
					<div class="glyphicon glyphicon-remove" style="cursor:pointer" ng-click="removeTask(task.id);$event.stopPropagation();"></div>
				</td>
				<!--<td>{{ task.is_finish == 1 ? 'Done' : 'In Progress' }} </td>-->
			</tr>
			</tbody>
		</table>
	</div>
	<div class="row">
		<p>Last Update: {{lastRefreshTime | date:'MMM d, y h:mm a'}}</p>
	</div>
	<div class="row">
		<detail-task data="show_task" edit-func="setCurrentEditTask(task)" delete-func="removeTask(id);" finish-func="markFinish(id,val)"></detail-task>
	</div>
	<div class="row">
		<editor add-task-func="addTask(pub_name,task_desc,priority)" edit-task-func="editTask(id,data)" data="currentEditTask" clear-form="clearForm"></editor>
	</div>
	<hr />
	<div class="row page-footer pull-right">
		Task-Manager by MagicJack v1.1.0
	</div>
</div>

</body>
<!--Modules-->
<script src="../angular/app.js"></script>

<!--Controllers-->
<script src="../angular/controllers/MainController.js"></script>

<!--Services-->
<script src="../angular/services/taskList.js"></script>

<!--Directives-->
<script src="../angular/directives/editor.js"></script>
<script src="../angular/directives/detailTask.js"></script>

<!-- JQuery Plugins-->
<script src="../js/jquery.toastmessage.js"></script>
<link type="text/css" href="../css/jquery.toastmessage.css" rel="stylesheet">
</html>