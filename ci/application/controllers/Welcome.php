<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
		parent::__construct();
		//Security checks
		$username = (isset($_COOKIE["username"]))?$_COOKIE["username"]:null;
		$token    = (isset($_COOKIE["token"]))?$_COOKIE["token"]:null;
		$this->load->model('member_model');
		if(empty($username) || !$username){exit(return_json(false,'not logged in missing username'));}
		if(empty($token) || !$token){exit(return_json(false,'missing token'));}
		if($this->member_model->token_validate($username,$token) !== true)
		{
			exit(return_json(false,"action not authorized, invalid token."));
		}
	}

	public function index()
	{
		$_data = array();
		$this->load->view('welcome_view',$_data);
	}

	public function editTask(){
		$this->load->model('task_model');
		$id = $this->input->post('id');
		$data = $this->input->post('data');
		//If there is a valid id
		if($this->task_model->taskExists($id)){
			//1 is to mark for finish otherwise mark for unfinished
			$result = $this->task_model->editTask($id,$data);
			if($result){
				exit(return_json('1',"Record id $id updated."));
			}else{
				exit(return_json('1',"Record id $id already updated."));
			}

		}
		$dataStr = json_encode($data);
		exit(return_json(0,"Missing ID or ID does not exists [id = '$id' | data = '$dataStr']"));
	}

	public function getTask(){
//		sleep(5);
		$this->load->model('task_model');
		//Set output type to json
		$this->output->set_content_type('application/json');
		//Exit and output the json data.
		exit(return_json(true,'',$this->task_model->getTaskList()));
	}

	public function finishMarker(){
		$this->load->model('task_model');
		$id = $this->input->post('id');
		//If there is a valid id
		if($this->task_model->taskExists($id)){
			//1 is to mark for finish otherwise mark for unfinished
			$finish = ($this->input->post('is_finish') == 1)?1:0;
			$result = $this->task_model->finishMarker($id,$finish);
			if($result){
				exit(return_json('1',"Record id $id updated."));
			}else{
				exit(return_json('1',"Record id $id already updated."));
			}

		}
		$finish = $this->input->post('is_finish');
		exit(return_json(0,"Missing ID or ID does not exists [id = '$id' | finish = '$finish']"));
	}

	public function addTask(){
		$this->load->model('task_model');
		$pub_name = $this->input->post('pub_name');
		$priority = $this->input->post('priority');
		$task_desc = $this->input->post('task_desc');
		$this->form_validation->set_rules('pub_name', 'Name', 'required');
		$this->form_validation->set_rules('task_desc', 'Descriptions', 'required');
		if ($this->form_validation->run() == FALSE){
			exit(return_json('0',validation_errors()));
		}else{
			$data = array(
				'pub_name' => $pub_name,
				'task_desc' => $task_desc,
				'priority' => $priority
			);
			$this->task_model->addTask($data);
			exit(return_json('1',$this->db->insert_id()));
		}
		return;
	}

	public function removeTask(){
		$this->load->model('task_model');

		$id = $this->input->post('id');
		$this->form_validation->set_rules('id', 'Task ID', 'required|integer');
		if ($this->form_validation->run() == FALSE){
			exit(return_json('0',validation_errors()));
		}else{
			if($this->task_model->removeTask($id) == 1){
				exit(return_json('1',"Task $id removed"));
			}
			exit(return_json('0',"Task $id not found"));
		}
		return;
	}

}
