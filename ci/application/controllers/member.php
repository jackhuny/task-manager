<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Member extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('member_model');
        $this->config->load('config_member',true);
    }
    function index()
    {
        var_dump($this->member_model->token_validate('jackhuny','c0c186070977e3f9fdcb317adb8d5f0c10a4860969694ef00625dd970542dd31'));
    }

    function member_signup()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<span>', '</span>');
        $this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[5]|max_length[12]|is_unique[members.username]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|matches[password_repeat]');
        $this->form_validation->set_rules('password_repeat', 'Password Confirmation', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[members.email]');
        if ($this->form_validation->run() == true)
        {
            $member_info = array(
                'username'=>$this->input->post('username'),
                'password'=>md5($this->input->post('password')),
                'email'=>$this->input->post('email'),
            );
            exit(json_encode( $this->member_model->member_add($member_info) ));
        }
        else
        {
            exit(json_encode( ['success'=>false, 'msg'=>validation_errors()] ));
        }
    }

    function member_login()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $remember = $this->input->post('remember');
        if(empty($username) || empty($password))
        {
            exit(json_encode( ['success'=>false, 'msg'=>'username or password not provided'] ));
        }
        $query = $this->member_model->member_validate($username,md5($password));
        if($query->success === true)
        {
            //Handle Success and session set
            $user_data = array(
                'username'  => $username,
                'token'     => $query->token,
                'logged_in' => TRUE
            );
            foreach($user_data as $k => $v)
            {
                if($remember)
                {
                    setcookie($k,$v,time()+$this->config->item('cookie_time'),'/');
                }
                else
                {
                    setcookie($k,$v,0,'/');
                }
            }
            exit(json_encode(['success'=>true,'token'=>$query->token]));
        }
        else
        {
            exit(json_encode( ['success'=>false, 'msg'=>$query->msg] ));
        }
    }

    function member_logout()
    {
        setcookie("username","",time()-1,'/');
        setcookie("token","",time()-1,'/');
        setcookie("logged_in","false",null,'/');
        exit(json_encode( ['success'=>true]));
    }

    function member_destroy_token()
    {
        $username = $this->input->cookie('username',true);
        $token    = $this->input->cookie('token',true);
        if($this->member_model->token_validate($username,$token))
        {
            $this->member_model->member_edit(['username'=>$username],['token'=>null]);
            $this->member_logout();
            exit(json_encode( ['success'=>true]));
        }
        else
        {
            exit(json_encode( ['success'=>false,'msg'=>'invalid username or token, token not destroyed']));
        }


    }


}