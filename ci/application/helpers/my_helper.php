<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('return_json'))
{
    function return_json($status=false,$msg='',$data='')
    {
        return json_encode(
            array(
                'status' => ($status)?1:0,
                'msg'    => $msg,
                'success'=> ($status==1||$status===true)?true:false,
                'data'   => $data
            )
        );
    }
}