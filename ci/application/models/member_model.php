<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Member_model extends CI_Model
    {
        function __construct()
        {
            parent::__construct();
            $this->config->load('config_member',false,false);
        }

        function member_get($search)
        {
            $query = $this->db->get_where('members',$search, 1, 0);
            $result = $query->result();
            if($result)
            {
                return (object) ['success'=>true,'data'=>$query->result()];
            }
            else
            {
                return (object) ['success'=>false];
            }
        }

        function member_edit($where,$data)
        {
            $this->db->update('members', $data, $where);
            return $this->affected_check();
        }

        /**Adding a new member.
         * @param (array) $member_info member information array
         * @return object success or fail (with msg) adding new member
         */
        function member_add($member_info)
        {
            $username = $member_info['username'];
            $min_username = $this->config->item('min_username');
            if(strlen($username) < $min_username)
            {
                return (object) ['success'=>false,'msg'=>"username \"$username\" is too short (min of $min_username)"];
            }
            $user = [ 'username'=>$member_info['username'] ];
            if($this->member_get($user)->success)
            {
                return (object) ['success'=>false,'msg'=>'username already exist'];
            }
            $this->db->set($member_info);
            $this->db->set('member_since','now()',false);
            $this->db->insert('members');
            return $this->affected_check();
        }

        function member_remove($id)
        {
            if($this->member_get(['id'=>$id])->success)
            {
                $this->db->from('members')->where('id', $id)->limit(1, 0)->delete();
                //Checking for delete success
                return $this->affected_check();
            }
            else
            {
                return (object) ['success'=>false , 'msg'=>'member id not found' ];
            }

        }

        function member_validate($username,$password)
        {
            $member_info = array
            (
                'username' => $username,
                'password' => $password
            );

            //Check if member exists
            //Comment out for more security
//            if($this->member_get(['username'=>$username])->success !== true)
//            {
//                return (object) ['success'=>false,'msg'=>'member not found'];
//            }
            //Check if member and password is correct
            if($this->member_get($member_info)->success !== true)
            {
                return (object) ['success'=>false,'msg'=>'member or password incorrect'];
            }

            //Check if there is a token
            $token = $this->member_get(['username'=>$username]);
            if($token->success && !empty($token->data[0]->token))
            {
                $token = $token->data[0]->token;
                return (object) ['success'=>true, 'token'=>$token];
            }

            //Token not exists gen token with time and username
            $token = hash('sha256', time().$username ,false);
            //Save token to db
            $query = $this->member_edit
            (
                ['username' => $username],
                ['token' => $token, 'token_time' => date("Y-m-d H:i:s")]
            );
            if($query->success === true)
            {
                return (object) ['success'=>true, 'token'=>$token];
            }
            else
            {
                return (object) ['success'=>false,'msg'=>'fail to set token'];
            }
        }

        function token_validate($username,$token)
        {
            $search = ['username'=>$username, 'token'=>$token];
            return $this->member_get($search)->success;
        }

        private function affected_check()
        {
            if($this->db->affected_rows() == 1)
            {
                return (object) ['success'=>true];
            }
            else
            {
                return (object) ['success'=>false];
            }
        }
    }