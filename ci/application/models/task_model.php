<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Task_model extends CI_Model
    {

        function __construct()
        {
            // Call the Model constructor
            parent::__construct();
        }

        function getTaskList(){
            //Get task list from the database
            $query = $this->db->get('tasks');
//            var_dump($this->db->last_query());
            return $query->result();
        }

        function taskExists($id){
            $query = $this->db->get_where('tasks',array('id' => $id), 1, 0);
            if($query->result()){
                return true;
            }else{
                return false;
            }
        }

        function editTask($id,$data){
            $this->db->from('tasks');
            $this->db->where('id',$id);
            $this->db->set($data);
            $this->db->limit(1);

            $this->db->update();
            return $this->db->affected_rows();
        }

        function finishMarker($id,$finish){
            //determine if task is to mark finish
            $finish = ($finish)?1:0;

            $this->db->from('tasks');
            $this->db->where('id',$id);
            $this->db->set('is_finish' , $finish);
            $this->db->set('finish_date','now()',false);
            $this->db->limit(1);

            $this->db->update();
            return $this->db->affected_rows();
        }

        function addTask($data){
//            var_dump($data);exit();
            $this->db->insert('tasks', $data);
            return ($this->db->affected_rows() != 1) ? false : true;
        }

        function removeTask($id){
            $this->db->where('id', $id);
            $this->db->delete('tasks');

            return $this->db->affected_rows();
        }
    }