describe("Main Controller", function() {

    beforeEach(module('taskApp'));

    var HelloWorldController, scope;

    beforeEach(inject(function ($rootScope, $controller) {
        scope = $rootScope.$new();
        HelloWorldController = $controller('MainController', {
            $scope: scope
        });
    }));
    it('Error Checking', function () {
        expect(scope.error).toEqual(false);
    });
});