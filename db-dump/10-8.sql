-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for task_db
CREATE DATABASE IF NOT EXISTS `task_db` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `task_db`;


-- Dumping structure for table task_db.tasks
CREATE TABLE IF NOT EXISTS `tasks` (
  `id` tinyint(10) unsigned NOT NULL AUTO_INCREMENT,
  `pub_name` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pub_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `task_desc` text COLLATE utf8_unicode_ci NOT NULL,
  `is_finish` tinyint(1) unsigned zerofill NOT NULL DEFAULT '0',
  `finish_date` datetime DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table task_db.tasks: ~8 rows (approximately)
/*!40000 ALTER TABLE `tasks` DISABLE KEYS */;
INSERT IGNORE INTO `tasks` (`id`, `pub_name`, `pub_date`, `task_desc`, `is_finish`, `finish_date`) VALUES
	(6, 'test_name', '2015-09-30 20:44:51', 'test_desc', 0, '0000-00-00 00:00:00'),
	(15, 'name', '2015-09-30 20:53:44', 'desc', 0, '0000-00-00 00:00:00'),
	(73, 'Jack', '2015-10-07 15:44:47', 'Work the magic', 0, '0000-00-00 00:00:00'),
	(74, 'Hide', '2015-10-07 15:45:21', 'ddd', 1, '0000-00-00 00:00:00'),
	(75, 'Bryan', '2015-10-07 15:46:58', 'Hihi', 1, '0000-00-00 00:00:00'),
	(76, 'Bryan', '2015-10-07 15:47:23', 'Hihi', 0, '0000-00-00 00:00:00'),
	(77, 'Jack', '2015-10-07 15:47:28', 'Hihi', 0, '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `tasks` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
