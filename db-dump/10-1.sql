-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for task_db
DROP DATABASE IF EXISTS `task_db`;
CREATE DATABASE IF NOT EXISTS `task_db` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `task_db`;


-- Dumping structure for table task_db.tasks
DROP TABLE IF EXISTS `tasks`;
CREATE TABLE IF NOT EXISTS `tasks` (
  `id` tinyint(10) unsigned NOT NULL AUTO_INCREMENT,
  `pub_name` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pub_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `task_desc` text COLLATE utf8_unicode_ci NOT NULL,
  `is_finish` tinyint(1) unsigned zerofill NOT NULL DEFAULT '0',
  `finish_date` datetime DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table task_db.tasks: ~9 rows (approximately)
/*!40000 ALTER TABLE `tasks` DISABLE KEYS */;
REPLACE INTO `tasks` (`id`, `pub_name`, `pub_date`, `task_desc`, `is_finish`, `finish_date`) VALUES
	(1, 'Jack', '2015-09-28 15:11:11', 'To Add More\r\n2nd line\r\n\r\n\r\n4rd line\r\n5th line', 1, '2015-09-28 15:15:15'),
	(3, 'Jack.Hu', '2015-09-28 15:30:57', 'To Add More V2.4', 0, '0000-00-00 00:00:00'),
	(6, 'test_name', '2015-09-30 20:44:51', 'test_desc', 0, NULL),
	(15, 'name', '2015-09-30 20:53:44', 'desc', 0, NULL),
	(32, 'fdsafa', '2015-09-30 22:08:29', 'asdfadsf\n\ndf\nsdf\nsdf\nsd\n\n\ndf', 0, NULL),
	(34, 'sdafasd', '2015-09-30 22:22:38', 'fasdfasdf', 0, NULL),
	(35, 'gogogo', '2015-09-30 23:10:22', 'gogogo', 0, NULL),
	(36, 'Jack The King', '2015-09-30 23:10:32', 'I am the king', 0, NULL),
	(37, 'dfasdf', '2015-09-30 23:11:25', 'asdf', 0, NULL);
/*!40000 ALTER TABLE `tasks` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
