'use strict';
taskApp.directive('member', ['memberFactory','$window','toastFactory',function(memberFactory,$window,toastFactory) {
    return {
        restrict: "EA",
        scope: {
        },
        templateUrl: 'angular/directives/member.html',
        controller: function($scope){
            $scope.member_login = function(form_data)
            {
                $('#member_login_modal').modal('hide');
                memberFactory.login(form_data).then(function(data)
                {
                    if(data.success === true){
                        $window.location.reload();
                    }
                    else
                    {
                        $('#member_login_modal').modal('show');
                        //todo handle login error
                        toastFactory.toast('error',data.msg,false,false);
                    }
                });
            };

            $scope.member_signup = function(form_data)
            {
                $('#loading_indicator.content').text('Signing Up... ');
                $('#loading_indicator').show('slow');
                if(typeof form_data == 'undefined') {
                    toastFactory.toast('error','please fill in the form',false,false,10000);
                }else{
                    memberFactory.signup(form_data).then(function(data)
                    {
                        if(data.success === true){
                            toastFactory.toast('info','successfully signup please login',false,false);
                            $('#member_signup_modal').modal('hide');
                            $('#member_login_modal').modal('show');
                        }
                        else
                        {
                            toastFactory.toast('error',data.msg,false,false,10000);
                        }

                    });
                }
                $('#loading_indicator.content').text('Loading... ');
                $('#loading_indicator').hide(0);
            }
        }
    };
}]);
