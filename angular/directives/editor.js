taskApp.directive('editor', function() {
    return {
        restrict: "EA",
        scope: {
            addTaskFunc:'&',
            editTaskFunc:'&',
            clearForm:'=',
            data:'='
        },
        templateUrl: 'angular/directives/editor.html',
        link : function(scope, el, attr){
            scope.resetForm = function(){
                scope.clearForm = true;
            }
            scope.$watch('clearForm',function(newVal,oldVal){
                if(newVal === true){
                    scope.a_pub_name = '';
                    scope.a_task_desc = '';
                    scope.e_pub_name = scope.data.pub_name;
                    scope.e_task_desc = scope.data.task_desc;
                    scope.addTaskForm.$setPristine();
                    scope.clearForm = false;
                }
            },true);

            scope.$watch('data',function(newVal,oldVal){
                //console.log(newVal);console.log('here');
                scope.e_pub_name = newVal.pub_name;
                scope.e_task_desc = newVal.task_desc;
                scope.e_task_priority = newVal.priority;
                scope.e_id = newVal.id;
            },true);
        },
        controller: function(){
        }
    };
});