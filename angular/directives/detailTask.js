taskApp.directive('detailTask', function() {
    return {
        restrict: "EA",
        scope: {
            data:'=',
            editFunc:'&',
            deleteFunc:'&',
            finishFunc:'&'
        },
        templateUrl: 'angular/directives/detailTask.html'
    };
});
