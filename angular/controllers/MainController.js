'use strict';
taskApp.controller('MainController',
    ['$scope','$interval', '$rootScope', 'loadStatusHolderFactory','errorStatusHolderFactory', 'taskFactory','toastFactory','$cookies','memberFactory',
    function($scope, $interval, $rootScope, loadStatusHolderFactory, errorStatusHolderFactory, taskFactory, toastFactory,$cookies,memberFactory) {
        //<editor-fold desc="scope vars">
        $scope.loadingStatusHolder = loadStatusHolderFactory.getHolder();
        $scope.errorStatusHolder = errorStatusHolderFactory.getHolder();
        $scope.refreshRate = 1500; //in milliseconds
        $scope.loading = true;
        $scope.error = false;
        $scope.error_text = null;
        $scope.clearForm = false;
        $scope.lastRefreshTime = '...';
        $scope.currentEditTask = '';
        //Pause the auto refresh(will NOT notify user status)
        //Stop the auto refresh(will notify user status)
        $scope.refreshCtrl = {pause:false,stop:true};
        $scope.toast = toastFactory;
        //</editor-fold>

        //Member Ctrl
        $scope.member_destroy_token = function(){memberFactory.removeMemberToken();toastFactory.toast('info','token remvoed & logged out',null,false,2000)};
        $scope.member_logout = function(){memberFactory.logout();$scope.refreshCtrl.stop = true;toastFactory.toast('info','logged out',null,false,2000)};
        $scope.member = $cookies.getAll();
        $scope.$watch
        (
            function() { return $cookies.getAll(); },
            function()
            {
                if(typeof $cookies.getAll() == 'undefined')
                {
                    $scope.member = null;
                }
                else
                {
                    $scope.member = $cookies.getAll('member');
                    //console.log('Cookie string: ' + $cookies.get('member'));
                }

            }
        );


        //If member logged start loading list.
        if('logged_in' in $scope.member && $scope.member.logged_in == true) {
            $("#loading_indicator").show();
            taskFactory.getResponse('g',{},false,$scope.refreshList)
                .then(function(data){
                    $scope.taskList = data;
                    //console.log($scope.taskList);
                    $scope.lastRefreshTime = new Date();
                    $("#loading_indicator").hide();
                });
        }

        $scope.removeTask = function(d_id){
            taskFactory.getResponse('r',d_id,false,$scope.refreshList,$scope.refreshCtrl).then();
        };
        $scope.addTask = function(pub_name,task_desc,priority){
            var addData = {pub_name:pub_name,task_desc:task_desc,priority:parseInt(priority)};
            taskFactory.getResponse('a',addData,false,$scope.refreshList).then(
                function(){
                    $('#addTaskModal').modal('hide');
                    $scope.clearForm = true;
                });
        };
        $scope.markFinish = function(id,finish){
            taskFactory.getResponse('m',{id:id,is_finish:finish},false,$scope.refreshList);
        };
        $scope.editTask = function(id,data){
            taskFactory.getResponse('e',{id:id,data:data},false,$scope.refreshList).then(
                function(){
                    $('#editTaskModal').modal('hide');
                    $scope.refreshList();
                }
            );
        };
        $scope.setCurrentEditTask= function(task){
            $scope.currentEditTask = task;
            $('#editTaskModal').modal('show');
        };

        //Auto refreshes ctrl
        $interval(function(){
            if(!$scope.refreshCtrl.pause && !$scope.refreshCtrl.stop){
                $scope.refreshList();
                //console.log('Refreshed');
            }
        },$scope.refreshRate);
        $scope.refreshList = function(silence){
            silence = (typeof silence === 'undefined')? true : silence;
            var promise = taskFactory.getResponse('g',{},true,$scope.refreshList,null);
            promise.then(
                function(response){
                    //Check for valid json data response
                    if(response instanceof Array){
                        $scope.newTaskList = response;
                        $scope.lastRefreshTime = new Date();
                        if( $scope.newTaskList !== undefined && (angular.toJson($scope.newTaskList) !== angular.toJson($scope.taskList)) ){
                            //TODO fix bug list does not updated after edit.
                            //Simply Replace the whole object may try to update changed only
                            $scope.taskList = $scope.newTaskList;
                            console.log('updated');
                            //Replace changes only.
                            //for (var key in $scope.taskList) {
                            //    if ($scope.taskList.hasOwnProperty(key)) {
                            //        if($scope.taskList[key] !== $scope.newTaskList[key]){
                            //            $scope.taskList[key] = $scope.newTaskList[key];
                            //        }
                            //    }
                            //}

                        }
                    }
                });
        };
        //Watch for refresh status change and notify user
        $scope.$watch(
            function(scope) { return scope.refreshCtrl.stop },
            function(nVal,oVal) {
                if (nVal !== oVal) {
                    var onOff = ($scope.refreshCtrl.stop)?'OFF':'ON';
                    toastFactory.toast('info',"Auto Refresh Tuned "+onOff,false,false);
                }
            }
        );

        //Show task detail
        $scope.showTask = function(task){
            $scope.show_task = task;
        };

        //Order and sort functions
        $scope.toJsDate = function(str){
            if(!str)return null;
            return new Date(str);
        };
        $scope.orderById = function(task){
            return parseInt(task.id);
        };
        $scope.orderByDate = function(task){
            return new Date(task.pub_date);
        };
        $scope.orderByDefault = ['priority',$scope.orderById];
        $scope.predicate = $scope.orderByDefault;
        $scope.reverse = true;
        $scope.customOrder = function(predicate) {
            $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
            $scope.predicate = predicate;
        };
    }]);