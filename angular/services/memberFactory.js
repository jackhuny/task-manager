'use strict';
taskApp.factory('memberFactory', function($q, $http, $cookies)
{
    return {
        signup: function (form_data) {
            var defer = $q.defer();
            if(typeof form_data == 'undefined'){form_data = {}}
            $http({
                method: 'post',
                url: 'ci/member/member_signup',
                headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                data: $.param(form_data)
            }).then(
                function (response) {
                    if (response.data instanceof Array || response.data instanceof Object) {
                        //Success logged in and set cookies
                        //$cookies.putObject('member',{'username':username,'token':response.data.token});
                        defer.resolve(response.data);
                    } else {
                        //Parse Error Handler
                        defer.reject(response);
                    }
                },
                function (response) {
                    //HTTP Error Handler
                    defer.reject(response);
                }
            );
            return defer.promise;
        },
        login : function(form_data)
        {
            var defer = $q.defer();
            if(typeof form_data == 'undefined'){form_data = {}}
            $http({
                method: 'post',
                url: 'ci/member/member_login',
                headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                data: $.param(form_data)
            }).then(
                function (response) {
                    if (response.data instanceof Array || response.data instanceof Object) {
                        //Success logged in and set cookies
                        //$cookies.putObject('member',{'username':username,'token':response.data.token});
                        defer.resolve(response.data);
                    } else {
                        //Parse Error Handler
                        defer.reject(response);
                    }
                },
                function (response) {
                    //HTTP Error Handler
                    defer.reject(response);
                }
            );
            return defer.promise;
        },

        logout : function ()
        {
            var defer = $q.defer();
            $http({
                method: 'post',
                url: 'ci/member/member_logout',
                headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
            }).then(
                function (response) {
                    if (response.data instanceof Array || response.data instanceof Object) {
                        defer.resolve(response.data);
                    } else {
                        //Parse Error Handler
                        defer.reject(response);
                    }
                },
                function (response) {
                    //HTTP Error Handler
                    defer.reject(response);
                }
            );
            return defer.promise;
        },

        removeMemberToken : function()
        {
            $http({
                method: 'post',
                url: 'ci/member/member_destroy_token',
                headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
            })
        }

    }
});
