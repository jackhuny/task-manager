'use strict';
taskApp.factory('loadStatusHolderFactory',function(){
    var loadStatusHolder = {};
    return{
        getHolder : function(){return loadStatusHolder;},
        setHolder : function(key,val){
            loadStatusHolder[key] = val;
        }
    };
});
taskApp.factory('errorStatusHolderFactory',function(){
    var errStatusHolder = {};
    return{
        getHolder : function(){return errStatusHolder;},
        setHolder : function(key,val){
            errStatusHolder[key] = val;
        }
    };
});
taskApp.factory('toastFactory',function(){
    return{
        /**
         * Sends out a toast message to notify user.
         * @param type Type of toast message ['success','error','notice','warning']
         * @param msg Toasted message
         * @param reload reload function or false
         * @param sticky true or false
         */
        toast :function(type, msg, reload, sticky, stayTime) {
            $().toastmessage('showToast', {
                text: msg,
                sticky: sticky,
                type: type,
                stayTime:stayTime,
                close: function () {
                    if (typeof reload == "function") {
                        reload;
                    }
                }
            });
        }
    }
});

taskApp.factory('taskFactory', function($q, $http, $timeout,toastFactory, loadStatusHolderFactory, errorStatusHolderFactory) {
    return {
        //Type [a=add,e=edit,m=mark,r=remove,g=get]
        getResponse : function(type,data,silence,reloadFunc,pack){
            var defer = $q.defer();
            var holderType,url,method;
            var param = {};
            var successHandle = function(response){};
            var parseError    = function(response){};
            var httpError     = function(response){};
            var finalHandle   = function(response){};

            //Init mode
            type = typeof type !== 'undefined' ? type.toLowerCase():'';

            switch(type){
                case 'a':
                    // <editor-fold desc="Add a Task">
                    method = "post";
                    url = 'ci/welcome/addTask';
                    holderType = 'addTask';
                    //Validate data
                    if(true){
                        param = data;
                    }else{
                        defer.reject();
                        return defer.promise;
                    }
                    successHandle = function(response){
                        if(response.data['status'] == 1){
                            errorStatusHolderFactory.setHolder(holderType,false);
                            loadStatusHolderFactory.setHolder(holderType,false);
                            if(!silence){ toastFactory.toast('success','successfully added a new task',false,false) }
                            reloadFunc();
                            defer.resolve(response.data);
                        }else{
                            var error_text  = 'failed to add task. error: '+response.data['msg'];
                            if(!silence){
                                errorStatusHolderFactory.setHolder(holderType,true);
                                toastFactory.toast('error',error_text,false,false);
                            }
                            defer.reject();
                        }
                    };
                    parseError = function(response){
                        console.log(response);
                        if(!silence){
                            errorStatusHolderFactory.setHolder(holderType,true);
                            toastFactory.toast('error','fail to add task, invalid response.',false,false);
                        }
                        defer.reject();
                    };
                    httpError = function(response){
                        console.log(response);
                        if(!silence){
                            errorStatusHolderFactory.setHolder(holderType,true);
                            toastFactory.toast('error','failed invalid response',false,false);
                        }
                        defer.reject();
                    };
                    finalHandle = function(response){};
                    break;
                    //</editor-fold>
                case 'e':
                    //<editor-fold desc="Edit a Case">
                    holderType  = 'editTask';
                    method      = 'post';
                    url         = 'ci/welcome/editTask';
                    param       =  data;
                    successHandle = function(response){
                        if(response.data['status'] == 1){
                            reloadFunc();
                            errorStatusHolderFactory.setHolder(holderType,false);
                            loadStatusHolderFactory.setHolder(holderType,false);
                            if(!silence){ toastFactory.toast('success','successfully edited task '+response.data['msg'],false,false) }
                            defer.resolve(response.data);
                        }else{
                            var error_text  = 'failed to edit task. error: '+response.data['msg'];
                            if(!silence){
                                errorStatusHolderFactory.setHolder(holderType,true);
                                toastFactory.toast('error',error_text,false,false);
                            }
                            defer.reject();
                        }
                    };
                    parseError = function(response){
                        console.log(response);
                        if(!silence){
                            errorStatusHolderFactory.setHolder(holderType,true);
                            toastFactory.toast('error','Unknown result, invalid response.',false,false);
                        }
                        defer.reject();
                    };
                    httpError = function(response){
                        console.log(response);
                        if(!silence){
                            errorStatusHolderFactory.setHolder(holderType,true);
                            toastFactory.toast('error','failed invalid response',false,false);
                        }
                        defer.reject();
                    };
                    finalHandle = function(response){};
                    break;
                    break;
                //</editor-fold>
                case 'r':
                    //<editor-fold desc="Remove a Case">
                    holderType = 'removeTask';
                    url = 'ci/welcome/removeTask';
                    method ='post';
                    param =  {id : data};
                    successHandle = function(response){
                        if(response.data['status'] == 1){
                            if(typeof pack.pause !== 'undefined'){
                                pack.pause = true;
                            }
                            errorStatusHolderFactory.setHolder(holderType,false);
                            loadStatusHolderFactory.setHolder(holderType,false);
                            if(!silence){ toastFactory.toast('success','successfully removed task '+data,false,false) }
                            $( "#task_"+data ).fadeOut("slow",function(){
                                if(typeof pack.pause !== 'undefined'){
                                    pack.pause = true;
                                }
                            });
                            defer.resolve(response.data);
                        }else{
                            var error_text  = 'failed to remove task. error: '+response.data['msg'];
                            if(!silence){
                                errorStatusHolderFactory.setHolder(holderType,true);
                                toastFactory.toast('error',error_text,false,false);
                            }
                            defer.reject();
                        }
                    };
                    parseError = function(response){
                        console.log(response);
                        if(!silence){
                            errorStatusHolderFactory.setHolder(holderType,true);
                            toastFactory.toast('error','Unknown remove result, invalid response.',false,false);
                        }
                        defer.reject();
                    };
                    httpError = function(response){
                        console.log(response);
                        if(!silence){
                            errorStatusHolderFactory.setHolder(holderType,true);
                            toastFactory.toast('error','failed invalid response',false,false);
                        }
                        defer.reject();
                    };
                    finalHandle = function(response){};
                    break;
                //</editor-fold>
                case 'm':
                    //<editor-fold desc="Mark a Task">
                    holderType  = 'markTask';
                    method      = 'post';
                    url         = 'ci/welcome/finishMarker';
                    param       =  data;
                    successHandle = function(response){
                        if(response.data['status'] == 1){
                            reloadFunc();
                            errorStatusHolderFactory.setHolder(holderType,false);
                            loadStatusHolderFactory.setHolder(holderType,false);
                            if(!silence){ toastFactory.toast('success','successfully marked task '+response.data['msg'],false,false) }
                            defer.resolve(response.data);
                        }else{
                            var error_text  = 'failed to mark task. error: '+response.data['msg'];
                            if(!silence){
                                errorStatusHolderFactory.setHolder(holderType,true);
                                toastFactory.toast('error',error_text,false,false);
                            }
                            defer.reject();
                        }
                    };
                    parseError = function(response){
                        console.log(response);
                        if(!silence){
                            errorStatusHolderFactory.setHolder(holderType,true);
                            toastFactory.toast('error','Unknown mark result, invalid response.',false,false);
                        }
                        defer.reject();
                    };
                    httpError = function(response){
                        console.log(response);
                        if(!silence){
                            errorStatusHolderFactory.setHolder(holderType,true);
                            toastFactory.toast('error','failed invalid response',false,false);
                        }
                        defer.reject();
                    };
                    finalHandle = function(response){};
                    break;
                    //</editor-fold>
                case 'g':
                    //<editor-fold desc="Get Task List">
                    method = "get";
                    url = 'ci/welcome/getTask';
                    holderType = 'getTask';
                    successHandle = function(response){
                        if(('success' in response.data) && response.data.success === false)
                        {
                            defer.reject(response);
                            errorStatusHolderFactory.setHolder('main',true);
                            toastFactory.toast('error', response.data.msg,reloadFunc,true);
                        }
                        else
                        {
                            defer.resolve(response.data.data);
                            errorStatusHolderFactory.setHolder(holderType,false);
                        }

                    };
                    parseError    = function(response){
                        if(!silence){
                            errorStatusHolderFactory.setHolder(holderType,true);
                            toastFactory.toast('error', 'Loaded data not valid',reloadFunc,true);
                        }
                        defer.reject(response);
                    };
                    httpError     = function(response){
                        if(!silence){
                            var error_text = 'Fail to load json. Error: ' + response.status + ' ' + response.statusText;
                            errorStatusHolderFactory.setHolder('main',true);
                            toastFactory.toast('error', error_text, reloadFunc, true);
                        }
                        defer.reject(response);
                    };
                    finalHandle   = function(response){};
                    break;
                    //</editor-fold>
                default :
                    defer.reject('Unknown action');
                    return defer.promise;
            }
            //Check if we are silenced
            if(!silence){
                loadStatusHolderFactory.setHolder(holderType, true);
                errorStatusHolderFactory.setHolder(holderType,false);
            }
            //Notify that we have started to process requests
            $timeout(
                function () {
                    defer.notify('Processing')
                },
                0
            );
            //http request
            $http({
                method: method,
                url: url,
                headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                data: $.param(param)
            }).then(
                function (response) {
                    if (response.data instanceof Array || response.data instanceof Object) {
                        successHandle(response);
                    } else {
                        //Parse Error Handler
                        parseError(response);
                        defer.reject(response);
                    }
                },
                function (response) {
                    //HTTP Error Handler
                    httpError(response);
                    defer.reject(response);
                }
            ).finally(
                function () {
                    //Finally Handler
                    finalHandle();
                    //Set False Loading Status
                    loadStatusHolderFactory.setHolder(holderType,false);
                }
            );

            return defer.promise;
        }
    }
});

//<editor-fold desc="Older Factorys">
//taskApp.factory('taskFactory', function($q, $http, $timeout,toastFactory, loadStatusHolderFactory, errorStatusHolderFactory) {
//    return {
//        getResponse : function(silence){
//            silence = typeof silence !== 'undefined' ? silence : false;
//            if(!silence){
//                loadStatusHolderFactory.setHolder('main', true);
//                errorStatusHolderFactory.setHolder('main',false)
//            }
//            var defer = $q.defer();
//            $timeout(
//                function () {
//                    defer.notify('About to get data')
//                },
//                0
//            )
//            $http.get('ci/welcome/getTask').then(
//                function (response) {
//                    if (response.data instanceof Array) {
//                        defer.resolve(response.data);
//                    } else {
//                        var error_text = 'Loaded data not valid';
//                        toastFactory.toast('error', error_text, true, true);
//                        console.log(response);
//                        defer.reject(response);
//                    }
//                },
//                function (response) {
//                    if(!silence){
//                        errorStatusHolderFactory.setHolder('main',false);
//                        var error_text = 'Fail to load json. Error: ' + response.status + ' ' + response.statusText;
//                        toastFactory.toast('error', error_text, true, true);
//                    }
//                    console.log(response);
//                    defer.reject(response);
//                }
//            ).finally(function () {
//                    if(!silence){
//                        loadStatusHolderFactory.setHolder('main', false);
//                    }
//                });
//            return defer.promise;
//        }
//    }
//});
//
//taskApp.factory('addTask', function($q, $http, $timeout,toastFactory, loadStatusHolderFactory, errorStatusHolderFactory){
//    return {
//        getResponse : function(pub_name,task_desc,silence,reloadFunc){
//            var error = false;
//            var error_text = '';
//            silence = typeof silence !== 'undefined' ? silence : false;
//            if(!silence){
//                loadStatusHolderFactory.setHolder('marker', true);
//                errorStatusHolderFactory.setHolder('marker',false);
//            }
//            var defer = $q.defer();
//            $timeout(
//                function () {
//                    defer.notify('About to get data')
//                },
//                0
//            )
//            $http({
//                method: 'post',
//                url: 'ci/welcome/addTask',
//                headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
//                data: $.param({ pub_name : pub_name, task_desc:task_desc })
//            }).then(
//                //Success
//                function (response) {
//                    if (typeof response.data === 'object') {
//                        if(response.data['status'] == 1){
//                            console.log('Success');
//                            reloadFunc();
//                            defer.resolve(false);
//                        }else{
//                            error = true;
//                            error_text += 'Fail to add task. error: '+response.data['msg']+'\n';
//                        }
//
//                    } else {
//                        error = true;
//                        error_text += 'Loaded data not valid.\n';
//                        console.log(response);
//
//                    }
//                },
//                //Failed
//                function (response) {
//                    error = true;
//                    error_text = 'Fail to load json. Error: ' + response.status + ' ' + response.statusText;
//                    console.log(response);
//                }
//            ).finally(function () {
//                    if(!silence){
//                        loadStatusHolderFactory.setHolder('marker', false);
//                        if(error){
//                            errorStatusHolderFactory.setHolder('marker',false);
//                            toastFactory.toast('error', error_text, true, false);
//                            defer.reject(response);
//                        }
//                    }
//                });
//            return defer.promise;
//        }
//    }
//});
//
//taskApp.factory('removeTask', function($q, $http, $timeout){
//    return {
//        getResponse : function(id){
//            var defer = $q.defer();
//            $timeout(
//                function () { defer.notify('About to get data') },
//                0
//            )
//            $http({
//                method: 'post',
//                url: 'ci/welcome/removeTask',
//                headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
//                data: $.param({ id: id })
//            }).then(
//                function (response) {
//                    console.log(response);
//                    defer.resolve(response);
//                },
//                function(response){
//                    defer.reject(response);
//                }
//            );
//            return defer.promise;
//        }
//    }
//});
//
//taskApp.factory('markTask',function($q, $http, $timeout, loadStatusHolderFactory, errorStatusHolderFactory, toastFactory){
//    return {
//        getResponse : function(silence,id,finish,reload){
//            var error = false;
//            var error_text;
//            silence = typeof silence !== 'undefined' ? silence : false;
//            if(!silence){
//                loadStatusHolderFactory.setHolder('marker', true);
//                errorStatusHolderFactory.setHolder('marker',false);
//            }
//            var defer = $q.defer();
//            $timeout(
//                function () {
//                    defer.notify('About to get data')
//                },
//                0
//            )
//            $http({
//                method: 'post',
//                url: 'ci/welcome/finishMarker',
//                headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
//                data: $.param({ id: id, finish:finish })
//            }).then(
//                function (response) {
//                    if (typeof response.data === 'object') {
//                        if(response.data['status'] == 1){
//                            console.log('Success');
//                            reload();
//                            defer.resolve(false);
//                        }else{
//                            error = true;
//                            error_text += 'Mark failed '+response.data['msg']+'\n';
//                        }
//
//                    } else {
//                        error = true;
//                        error_text += 'Loaded data not valid.\n';
//                        console.log(response);
//
//                    }
//                },
//                function (response) {
//                    error = true;
//                    error_text = 'Fail to load json. Error: ' + response.status + ' ' + response.statusText;
//                    console.log(response);
//                }
//            ).finally(function () {
//                    if(!silence){
//                        loadStatusHolderFactory.setHolder('marker', false);
//                        if(error){
//                            errorStatusHolderFactory.setHolder('marker',false);
//                            toastFactory.toast('error', error_text, true, false);
//                            defer.reject(response);
//                        }
//                    }
//                });
//            return defer.promise;
//        }
//    }
//});
//</editor-fold>